/**
 * Separation Degrees Media
 *
 * catalog.js
 *
 * @author    Separation Degrees <magento@separationdegrees.com>
 * @category  Separation Degrees Media
 * @package   sdm
 * @copyright Copyright (c) 2015 Separation Degrees Media (http://www.separationdegrees.com)
 */
(function($) {
    $.noConflict();
    $(document).ready(function() {
        if (AdjnavBlock) {
            AdjnavBlock.prototype.hideProducts = function() {
                if (this.checkIfExists()) {
                    var items = $(this.blockID).select('a', 'input', 'select');
                    for (var i=0; i<items.length; ++i){
                        items[i].addClassName('adj-nav-disabled');
                    }

                    if (typeof(adj_slider) != 'undefined')
                        adj_slider.setDisabled();

                    // var divs = $$('div.adj-nav-progress');
                    // for (var i=0; i<divs.length; ++i)
                    //     divs[i].show();

                    jQuery(".main-container")
                        .addClass('catalog-loading')
                        .append("<div class='sdo-catlog-ajax-loader'></div>");
                }
            };
            AdjnavBlock.prototype.showProducts = function() {
                if (this.checkIfExists()) {
                    var items = $(this.blockID).select('a','input', 'select');
                    for (var i=0; i<items.length; ++i){
                        items[i].removeClassName('adj-nav-disabled');
                    }
                    if (typeof(adj_slider) != 'undefined')
                        adj_slider.setEnabled();

                    jQuery(".main-container")
                        .removeClass('catalog-loading')
                        .find(".sdo-catlog-ajax-loader")
                        .remove();
                }
            };
        }
    });
})(jQuery);
