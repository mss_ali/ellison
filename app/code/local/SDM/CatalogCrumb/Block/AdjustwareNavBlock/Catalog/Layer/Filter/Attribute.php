<?php
/**
 * Separation Degrees One
 *
 * Custom breadcrumb functionality for Ellison's catalog
 *
 * PHP Version 5
 *
 * @category  SDM
 * @package   SDM_CatalogCrumb
 * @author    Separation Degrees One <magento@separationdegrees.com>
 * @copyright Copyright (c) 2015 Separation Degrees One (http://www.separationdegrees.com)
 */

/**
 * SDM_CatalogCrumb_Block_AdjustwareNavBlock_Catalog_Layer_Filter_Attribute class
 */
class SDM_CatalogCrumb_Block_AdjustwareNavBlock_Catalog_Layer_Filter_Attribute
    extends AdjustWare_Nav_Block_Catalog_Layer_Filter_Attribute
{
    /**
     * Array of cached items
     *
     * @var array
     */
    protected $_cachedItems = null;

    /**
     * Build an array of sub attributes to attributes
     *
     * @var array
     */
    protected $_checkSubAttributeFilter = array(
        'tag_subcategory'       => 'tag_category',
        'tag_subtheme'          => 'tag_theme',
        'tag_subcurriculum'     => 'tag_curriculum',
        'tag_subproduct_line'   => 'tag_product_line'
    );

    /**
     * Add "check sub attribute filter" value on construct
     */
    public function __construct()
    {
        parent::__construct();
        $this->setCheckSubAttributeFilter(true);
    }

    /**
     * Add crumbs to URL parameters
     *
     * @return array
     */
    public function getItemsArray()
    {
        if ($this->_cachedItems === null) {
            $items = parent::getItemsArray();

            $crumbSegment = "crumb=".Mage::getSingleton('sdm_catalogcrumb/crumb')->getHash();

            foreach ($items as $key => $item) {
                $itemHref = array();

                // Get the HREF
                preg_match('/<a[^>]* href="([^"]*)"/', $item, $itemHref);
                if (count($itemHref) < 2) {
                    continue;
                }

                // Add the crumb segment
                $oldHref = $itemHref[1];
                $parts = parse_url($oldHref);
                $newQuery = $parts['query'] . ($parts['query'] ? "&" : "") . "crumb=" . $crumbSegment;
                $items[$key] = str_replace($parts['query'], $newQuery, $item);
            }

            $this->_cachedItems = $items;
        }

        return $this->_cachedItems;
    }

    /**
     * Checker
     *
     * @return boolean
     */
    public function checkSubAttributeFilter()
    {
        $code = $this->getAttributeModel()->getAttributeCode();
        $filterCheck = $this->getRequest()->getParam($code);
        $filterCheck = $filterCheck === 'clear' ? null : $filterCheck;

        // If we've selected this sub filter, or if this is not a
        // sub filter, then skip checking parent filter
        if (empty($filterCheck) && isset($this->_checkSubAttributeFilter[$code])) {
            // Make sure parent filter is not empty
            $filterCheck = $this->getRequest()->getParam(
                $this->_checkSubAttributeFilter[$code]
            );
            $filterCheck = $filterCheck === 'clear' ? null : $filterCheck;
            return !empty($filterCheck);
        }
        return true;
    }
}
