<?php
/**
 * Separation Degrees One
 *
 * Magento catalog customizations
 *
 * PHP Version 5
 *
 * @category  SDM
 * @package   SDM_Catalog
 * @author    Separation Degrees One <magento@separationdegrees.com>
 * @copyright Copyright (c) 2015 Separation Degrees One (http://www.separationdegrees.com)
 */

/**
 * SDM_Catalog_Model_Rewrite_CatalogIndexMysql4Attribute class
 */
class SDM_Catalog_Model_Rewrite_CatalogIndexMysql4Attribute
    extends AdjustWare_Nav_Model_Rewrite_CatalogIndexMysql4Attribute
{
    /**
     * Separates counts' calculation for configurable and other products.
     *
     * Rewritten to add the type_id filter for the layered navigation attribute
     * option counts.
     *
     * @param Mage_Catalog_Model_Resource_Eav_Attribute $attribute
     * @param Varien_Db_Select                          $entitySelect
     *
     * @return void
     */
    public function getCount($attribute, $entitySelect)
    {
        $select = clone $entitySelect;
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->reset(Zend_Db_Select::ORDER);
        $select->reset(Zend_Db_Select::LIMIT_COUNT);
        $select->reset(Zend_Db_Select::LIMIT_OFFSET);
        $select->reset(Zend_Db_Select::DISTINCT);
        $select->reset(Zend_Db_Select::GROUP);


        $fields = array(
            'count'   => 'COUNT(DISTINCT index.entity_id)',
            'index.value',
            'type_id' => '("other")',
            );

        $select->columns($fields)
            ->join(array('index' => $this->getMainTable()), 'index.entity_id = e.entity_id', array())
            ->where('index.store_id = ?', $this->getStoreId())
            ->where('index.attribute_id = ?', $attribute->getId())
            ->group('index.value');

        // Start rewrite
        $select->where('`e`.`type_id` = "' . Mage::helper('sdm_catalog')->getCatalogFilterType() . '"');
        // End of rewrite

        $select=Mage::helper('adjnav')->addManufacturerFilter($select);

        $configurableSelect = $this->_getCountForConfigurable($attribute, $select, $entitySelect);

        $select = '('.$select->__toString().')';
        if ($configurableSelect) {
            $select = ' SELECT SUM(t.count) AS count, t.value '.
                ' FROM ('.$select.' UNION ('.$configurableSelect.')) AS t '.
                ' GROUP BY t.value ';
        }

        $result = $this->_getReadAdapter()->fetchAll($select);

        $counts = array();
        foreach ($result as $row) {
            $counts[$row['value']] = $row['count'];
        }

        return $counts;
    }
}
